package cn.gtcommunity.epimorphism.api.capability;

public interface IHeatExchanger {
    int getThrottle();
    int getParallel();
}
